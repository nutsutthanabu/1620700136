﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAutoFire : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform enemyBulletSpawner;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip bulletSound;

    [SerializeField] private float soundVolume;

    private float timeCount = 0;
    private float timerCount = 1;

    void Update()
    {
        AutoFire();
    }

    private void AutoFire()
    {
        timeCount -= Time.deltaTime;
        if (timeCount <= 0)
        {
            Instantiate(bullet, enemyBulletSpawner.position, enemyBulletSpawner.rotation);
            timeCount = timerCount;

            audioSource.PlayOneShot(bulletSound, soundVolume);
        }
    }
}
