﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShipHPManager : MonoBehaviour
{
    [SerializeField] private GameObject loseGameUI;
    [SerializeField] private GameObject enemyShip;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject hpUI;


    [SerializeField] private Text hp;

    [SerializeField] private int playerHP = 1000;
    private int enemyDamage = 10;
    private int timeFreeze = 0;

    private void Start()
    {
        hp.text = "Player HP : " + playerHP;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            playerHP -= enemyDamage;

            hp.text = "Player HP : " + playerHP;

        }

        if (playerHP <= 0)
        {
            hpUI.SetActive(false);
            playerShip.SetActive(false);
            Time.timeScale = timeFreeze;
            loseGameUI.SetActive(true);
        }

    }
}
