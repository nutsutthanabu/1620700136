﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHPManager : MonoBehaviour
{
    [SerializeField] private GameObject winGameUI;
    [SerializeField] private GameObject enemyShip;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private GameObject nextLevelUI;

    [SerializeField] private Text hp;

    [SerializeField] private int enemyHP = 100;

    private int playerDamage = 10;
    private int playerSecondBulletDamage = 15;
    private int timeFreeze = 0;

    

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "PlayerBullet")
        {
            enemyHP -= playerDamage;
            Debug.Log("HP: " + enemyHP);
            hp.text = "Enemy HP : " + enemyHP;

        }
        if (other.gameObject.tag == "PlayerBullet2")
        {
            enemyHP -= playerSecondBulletDamage;
            Debug.Log("HP: " + enemyHP);
            hp.text = "Enemy HP : " + enemyHP;

        }

        if (enemyHP <= 0)
        {
            hpUI.SetActive(false);
            enemyShip.SetActive(false);
            Time.timeScale = timeFreeze;

            nextLevelUI.SetActive(true);
        }

    }
}
