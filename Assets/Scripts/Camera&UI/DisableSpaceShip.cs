﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableSpaceShip : MonoBehaviour
{
    [SerializeField] private GameObject enemyShip;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject startGameUI;
    [SerializeField] private GameObject winGameUI;
    [SerializeField] private GameObject loseGameUI;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private GameObject nextLevelUI;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip menuSound;

    [SerializeField] private float soundVolume;

    private float timeFreeze = 0;

    void Start()
    {
        boss.SetActive(false);
        enemyShip.SetActive(false);
        playerShip.SetActive(false);
        winGameUI.SetActive(false);
        loseGameUI.SetActive(false);
        hpUI.SetActive(false);
        nextLevelUI.SetActive(false);

        startGameUI.SetActive(true);

        Time.timeScale = timeFreeze;

    }

    void Update()
    {
        if(startGameUI.active == true)
        {
            audioSource.PlayOneShot(menuSound,soundVolume);
        }
        if(startGameUI.active == false)
        {
            audioSource.Stop();
        }
    }
}
