﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class PlayerShipController : MonoBehaviour
{

    [SerializeField] private float playerShipSpeed;

    private Vector2 movementInput = Vector2.zero;
    private Vector2 playerInputInfo = Vector2.zero;

    private ShipInputActions inputActions;

    void Update()
    {
        Movement();
        SetBoundary();
    }

    private void Movement()
    {
        playerInputInfo = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        playerInputInfo = playerInputInfo.normalized;
        var newX = transform.position.x + playerInputInfo.x * Time.deltaTime * playerShipSpeed;
        var newY = transform.position.y + playerInputInfo.y * Time.deltaTime * playerShipSpeed;

        transform.position = new Vector2(newX, newY);
    }

    private void SetBoundary()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -9.5f, 9.5f), Mathf.Clamp(transform.position.y, -4, 4), transform.position.z);
    }
}
