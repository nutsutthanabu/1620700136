﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickToStartGame : MonoBehaviour
{
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject enemyShip;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject startGameUI;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private GameObject nextLevelUI;

    private float normalTimeScale = 1;

    void Start()
    {
        Button button = gameObject.GetComponent<Button>();

        button.onClick.AddListener(ChangeToGame);
    }

    private void ChangeToGame()
    {
        hpUI.SetActive(true);
        enemyShip.SetActive(true);
        playerShip.SetActive(true);

        nextLevelUI.SetActive(false);
        startGameUI.SetActive(false);
        boss.SetActive(false);

        Time.timeScale = normalTimeScale;
    }
}
