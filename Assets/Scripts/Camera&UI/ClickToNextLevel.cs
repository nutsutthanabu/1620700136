﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ClickToNextLevel : MonoBehaviour
{
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject enemyShip;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject startGameUI;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private GameObject nextLevelUI;
    [SerializeField] private Text bossHP;

    private float normalTimeScale = 1;
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();

        button.onClick.AddListener(NextLevel);
    }

    private void NextLevel()
    {
        hpUI.SetActive(true);
        boss.SetActive(true);
        playerShip.SetActive(true);

        nextLevelUI.SetActive(false);
        startGameUI.SetActive(false);
        enemyShip.SetActive(false);

        bossHP.text = "Enemy HP : 150";

        Time.timeScale = normalTimeScale;
    }
}
