﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{

    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(ResetGame);
    }

    private void ResetGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
