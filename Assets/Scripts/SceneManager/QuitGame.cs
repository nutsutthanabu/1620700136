﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitGame : MonoBehaviour
{
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(Quit);
    }

    private void Quit()
    {
        Application.Quit();
        Debug.Log("Game Quitted : Thanks For Playing!");
    }

}
