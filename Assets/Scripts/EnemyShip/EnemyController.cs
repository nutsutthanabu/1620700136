﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] Transform playerTransform;
    [SerializeField] float enemyShipSpeed;

    private float setDistanceFromPlayer = 1.5f;

    void Update()
    {
        TransformToPlayerShip();
    }

    private void TransformToPlayerShip()
    {
        Vector2 distanceToPlayerShip = playerTransform.position - transform.position;

        Vector2 directionToPlayerShip = distanceToPlayerShip.normalized;

        Vector2 enemySpeed = directionToPlayerShip * enemyShipSpeed;

        if(distanceToPlayerShip.magnitude > setDistanceFromPlayer)
        {
            transform.Translate(enemySpeed * Time.deltaTime);
        }
    }
}
