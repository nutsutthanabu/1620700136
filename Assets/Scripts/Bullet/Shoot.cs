﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private float fireRate;
    [SerializeField] private float fireRate2;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject bullet2;
    [SerializeField] private Transform bulletSpawner;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip bulletSound;
    [SerializeField] private AudioClip secondBulletSound;

    [SerializeField] private float soundVolume;

    private float nextFire;
    void Update()
    {
        if(Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(bullet, bulletSpawner.position, bulletSpawner.rotation);
            audioSource.PlayOneShot(bulletSound, soundVolume);
        }

        if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate2;
            Instantiate(bullet2, bulletSpawner.position, bulletSpawner.rotation);

            audioSource.PlayOneShot(secondBulletSound, soundVolume);
        }
    }
}
