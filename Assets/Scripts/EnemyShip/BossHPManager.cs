﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHPManager : MonoBehaviour
{
    [SerializeField] private GameObject winGameUI;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private GameObject nextLevelUI;

    [SerializeField] private Text hp;

    [SerializeField] private int bossHP = 200;

    private int playerDamage = 10;
    private int playerSecondBulletDamage = 15;
    private int timeFreeze = 0;

   
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "PlayerBullet")
        {
            bossHP -= playerDamage;
            Debug.Log("HP: " + bossHP);
            hp.text = "Enemy HP : " + bossHP;

        }
        if (other.gameObject.tag == "PlayerBullet2")
        {
            bossHP -= playerSecondBulletDamage;
            Debug.Log("HP: " + bossHP);
            hp.text = "Enemy HP : " + bossHP;

        }

        if (bossHP <= 0)
        {
            hpUI.SetActive(false);
            boss.SetActive(false);
            Time.timeScale = timeFreeze;

            winGameUI.SetActive(true);
        }

    }
}
